//
//  ViewController.swift
//  Magic8Ball
//
//  Created by Amal Babu on 05/12/22.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    
    let ballArray  = ["ball1", "ball2", "ball3", "ball4", "ball5"]
    var randomBallNumber : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        randomBallNumber = Int(arc4random_uniform(4))
        imageView.image = UIImage(named: ballArray[randomBallNumber])
    }


    @IBAction func askButtonPessed(_ sender: Any) {
        methodName()
        
    }
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        
       methodName()
    }
    
    func methodName()
      {
          randomBallNumber = Int(arc4random_uniform(4))
          imageView.image = UIImage(named: ballArray[randomBallNumber])
      }
    
}

